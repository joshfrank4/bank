package de.johjoh.bank;

import java.sql.Connection;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import de.johjoh.bank.cmd.BankCommand;
import de.johjoh.bank.listener.JoinQuitListener;
import de.johjoh.bank.listener.PlayerListener;
import de.johjoh.bank.sql.MySQLConnectionUtils;
import de.johjoh.bank.sql.MySQLUtil;
import de.johjoh.bank.user.User;
import de.johjoh.bank.user.UserStorage;
import de.johjoh.bank.util.BankUtil;
import de.johjoh.bank.util.InterestTask;
import net.milkbowl.vault.permission.Permission;

public class Bank extends JavaPlugin {

	public static final String PREFIX = ChatColor.GRAY + "[" + ChatColor.GREEN + "Bank" + ChatColor.GRAY + "] " + ChatColor.WHITE;
	public static final String CONSOLE_PREFIX = "[Bank] ";
	
	public static final String TABLE_USER = "bank_user";
	public static final String TABLE_ACCOUNTS = "bank_accounts";
	public static final String TABLE_TRANSACTIONS = "bank_transactions";
	
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_ACCOUNT_ID = "account_id";
	public static final String COLUMN_OWNER = "owner";
	public static final String COLUMN_BALANCE = "balance";
	public static final String COLUMN_CASH = "cash";
	public static final String COLUMN_ACCOUNTS = "accounts";
	public static final String COLUMN_TRANSACTIONS = "transactions";
	public static final String COLUMN_SENDER_ID = "sender_id";
	public static final String COLUMN_TARGET_ID = "target_id";
	public static final String COLUMN_VALUE = "value";
	
	private static Bank instance;
	
	private boolean vaultEnabled;
	private boolean loadError;
	
	private double monster_kill;
	private double animal_feed;
	private double animal_kill_costs;
	private double eat_meat;
	private double zinsen;
	
	private Permission permission = null;
	
	public double getMonsterKillReward() { return monster_kill; }
	public double getAnimalFeedReward() { return animal_feed; }
	public double getAnimalKillCosts() { return animal_kill_costs; }
	public double getEatMeatCosts() { return eat_meat; }
	public double getZinsen() { return zinsen; }
	
	public Permission getPermission() { return permission; }
	
	@Override
	public void onEnable() {
		
		instance = this;
		
		//	Checking for Vault
		vaultEnabled = vaultEnabled();
		if(!vaultEnabled) {
			loadError = true;
			System.out.println(CONSOLE_PREFIX + "Please install Vault before you use this plugin!");
			return;
		}
		
		//	Setting up config.yml
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		//	Loading config values
		monster_kill = getConfig().getDouble("monster_kill");
		animal_feed = getConfig().getDouble("animal.feed");
		animal_kill_costs = getConfig().getDouble("animal.kill_costs");
		eat_meat = getConfig().getDouble("eat_meat");
		zinsen = getConfig().getDouble("zinsen");
		
		//	Loading MySQL-Values
		if(!MySQLConnectionUtils.reloadValuesFromConfig()) {
			loadError = true;
			System.out.println(CONSOLE_PREFIX + "Error while loading MySQL-Properties!");
			System.out.println(CONSOLE_PREFIX + "Pls check the values!");
			return;
		}
		
		//	Setting up MySQL
		Connection con = null;
		
		try {
			con = MySQLConnectionUtils.getNewConnection();
		} catch (SQLException e) {
			loadError = true;
			System.out.println(CONSOLE_PREFIX + "Can't create MySQL-Connection.");
			System.out.println(CONSOLE_PREFIX + "Please check the values at the config.yml!");
			MySQLConnectionUtils.closeConnection(con);
			return;
		}
		
		try {
			if(!MySQLConnectionUtils.isConnectionValid(con)) {
				loadError = true;
				System.out.println(CONSOLE_PREFIX + "Can't create MySQL-Connection.");
				System.out.println(CONSOLE_PREFIX + "Please check the values at the config.yml!");
				MySQLConnectionUtils.closeConnection(con);
				return;
			}
			
		} catch (SQLException e) {}
		
		try {
			MySQLUtil.createTables(con);
		} catch (SQLException e) {
			loadError = true;
			System.out.println(CONSOLE_PREFIX + "Can't create tables!");
			MySQLConnectionUtils.closeConnection(con);
			return;
		}
		
		//	Loading Online-Users
		try {
			for(Player p : Bukkit.getOnlinePlayers()) {
				BankUtil.registerPlayer(p.getUniqueId(), p.getName(), con);
				UserStorage.registerUser(new User(p.getUniqueId(), con));
			}
		} catch (SQLException e) {
			loadError = true;
			MySQLConnectionUtils.closeConnection(con);
			System.out.println(CONSOLE_PREFIX + "Can't load User!");
			return;
		}
		
		MySQLConnectionUtils.closeConnection(con);
		
		//	Registrier Listener
		new JoinQuitListener();
		new PlayerListener();
		
		//	Registrier Commands
		getCommand("bank").setExecutor(new BankCommand());
		
		//	Start InterestTask
		InterestTask.runTask();
		
		//	Setting up Vault
		setupPermissions();
		
		System.out.println(CONSOLE_PREFIX + "Plugin Enabled!");
	}
	
	@Override
	public void onDisable() {
		
		System.out.println(CONSOLE_PREFIX + "Plugin Disabled!");
	}
	
	//	Checks for Vault
	private boolean vaultEnabled() {
		try {
			Class.forName("net.milkbowl.vault.Vault");
			return true;
		} catch (ClassNotFoundException e) {
			return false;
		}
	}
	
	//	Setting up Vault
	private boolean setupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if(permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		return (permission != null);
	}
	
	public boolean hasLoadError() { return loadError; }
	
	public static Bank getInstance() { return instance; }

}
