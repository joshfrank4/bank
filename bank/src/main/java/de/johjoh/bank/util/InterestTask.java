package de.johjoh.bank.util;

import java.sql.Connection;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import de.johjoh.bank.Bank;
import de.johjoh.bank.sql.MySQLConnectionUtils;
import de.johjoh.bank.user.User;
import de.johjoh.bank.user.UserStorage;

public class InterestTask {
	
	private static int task;
	
	public static void runTask() {
		//	Start Scheduler for Interests
		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Bank.getInstance(), new Runnable() {
			
			public void run() {
				Connection con = null;
				try {
					con = MySQLConnectionUtils.getNewConnection();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				//	Paying Money to all Online-Players
				for(User user : UserStorage.getUsers()) {
					double payed = user.payInterests(con);
					user.getPlayer().sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast deine Zinsen im Wert von " + ChatColor.YELLOW + payed + ChatColor.GREEN + " erhalten.");
				}
				MySQLConnectionUtils.closeConnection(con);
				
				System.out.println(Bank.CONSOLE_PREFIX + "Paying Interests!");
			}
		}, 5*60*20, 5*60*20);
	}
	
	public static int getTaskId() {
		return task;
	}

}
