package de.johjoh.bank.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import de.johjoh.bank.Bank;
import de.johjoh.bank.sql.MySQLUtil;

public class BankUtil {
	
	//	Create an copy of an List
	public static <T> List<T> copy(Collection<T> copyOf) {
		List<T> list = new ArrayList<T>();
		list.addAll(copyOf);
		return list;
	}
	
	//	Round an Double for x decimals
	public static double round(double value, int places) {
		long factor = (long) Math.pow(10, 2);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}
	
	//	Convert an Integer Account-ID to String
	public static String getIdToString(int id) {
		String s = "";
		for(int i = 6; i > String.valueOf(id).length(); i--) {
			s += "0";
		}
		s+=id;
		return s;
	}
	
	//	Convert an String Account-ID to Integer
	public static int idFromString(String id) {
		boolean b = false;
		String i = "";
		for(Character c : id.toCharArray()) {
			if(c.equals("0")) { if(b) { i+=c; }
			}
			else {
				b = true;
				i+=c;
			}
		}
		return Integer.valueOf(i);
	}
	
	//	Returns Inventory-Size for x Items
	public static int getInvSize(int itemAmount) {
		if(itemAmount > 45) return 54;
		if(itemAmount > 36) return 45;
		if(itemAmount > 27) return 36;
		if(itemAmount > 18) return 27;
		if(itemAmount > 9) return 18;
		if(itemAmount > 0) return 9;
		return 27;
	}
	
	//	Insert an Player into Database
	public static void registerPlayer(UUID uniqueId, String name, Connection con) throws SQLException {
		ResultSet rs = MySQLUtil.selectAll(Bank.TABLE_USER, Bank.COLUMN_UUID, uniqueId.toString(), con);
		if(!rs.next()) 
			con.prepareStatement("INSERT INTO `" + Bank.TABLE_USER + "`(`" + Bank.COLUMN_UUID + "`, `" + Bank.COLUMN_NAME + "`) VALUES ('" + uniqueId.toString() + "', '" + name + "')").execute();
		MySQLUtil.updateString(Bank.TABLE_USER, Bank.COLUMN_NAME, name, Bank.COLUMN_UUID, uniqueId.toString(), con);
	}
	
	public static ItemStack nextItem(int page) {
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta meta = (SkullMeta) item.getItemMeta();
		meta.setOwner("MHF_ArrowRight");
		meta.setDisplayName(ChatColor.GREEN + "Seite " + page);
		item.setItemMeta(meta);
		return item;
	}

}
