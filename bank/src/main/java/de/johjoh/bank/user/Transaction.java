package de.johjoh.bank.user;

public class Transaction {

	private int sender_id;
	private int target_id;
	private double value;
	
	public Transaction(int sender_id, int target_id, double value) {
		this.sender_id = sender_id;
		this.target_id = target_id;
		this.value = value;
	}

	public int getSenderId() { return sender_id; }
	public int getTargetId() { return target_id; }
	public double getValue() { return value; }

}
