package de.johjoh.bank.user;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import de.johjoh.bank.Bank;
import de.johjoh.bank.sql.MySQLConnectionUtils;
import de.johjoh.bank.sql.MySQLUtil;
import de.johjoh.bank.util.BankUtil;

public class Account {
	
	private int id = -1;
	private UUID owner = null;
	private double balance = 0.0D;
	private int transactionAmount = 0;
	
	public Account(int id, Connection con) {
		this.id = id;
		try {
			ResultSet rs = MySQLUtil.selectAll(Bank.TABLE_ACCOUNTS, Bank.COLUMN_ACCOUNT_ID, id, con);
			while(rs.next()) {
				owner = UUID.fromString(rs.getString(Bank.COLUMN_OWNER));
				balance = rs.getDouble(Bank.COLUMN_BALANCE);
				transactionAmount = rs.getInt(Bank.COLUMN_TRANSACTIONS);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int getId() { return id; }
	public UUID	 getOwner() { return owner; }
	public User getUser() { return UserStorage.getUser(owner); }
	public double getBalance() { return balance; }
	public int getTranactionsAmount() { return transactionAmount; }
	public String getIdToString() {
		String s = "";
		for(int i = 6; i > String.valueOf(id).length(); i--) {
			s += "0";
		}
		s+=id;
		return s;
	}
	
	public boolean addBalance(double amount) {
		Connection con = null;
		
		try {
			con = MySQLConnectionUtils.getNewConnection();
			
			boolean b = addBalance(amount, con);
			MySQLConnectionUtils.closeConnection(con);
			return b;
		} catch (SQLException e) {
			e.printStackTrace();
			MySQLConnectionUtils.closeConnection(con);
			return false;
		}
	}
	
	public boolean addBalance(double amount, Connection con) {
		boolean b = true;
		
		try {
			MySQLUtil.updateDouble(Bank.TABLE_ACCOUNTS, Bank.COLUMN_BALANCE, BankUtil.round((double) (balance + amount), 2), Bank.COLUMN_ACCOUNT_ID, String.valueOf(id), con);
		} catch (SQLException e) {
			e.printStackTrace();
			b = false;
		}
		
		if(b) balance = BankUtil.round(balance + amount, 2);
		if(getUser() != null) {
			getUser().sendScoreboard();
			getUser().updateAccountInventorys();
		}
		return b;
	}
	
	public boolean removeBalance(double amount) {
		if(amount > balance) return false;
		Connection con = null;
		
		try {
			con = MySQLConnectionUtils.getNewConnection();
			
			boolean b = removeBalance(amount, con);
			MySQLConnectionUtils.closeConnection(con);
			return b;
		} catch (SQLException e) {
			e.printStackTrace();
			MySQLConnectionUtils.closeConnection(con);
			return false;
		}
	}
	
	public boolean removeBalance(double amount, Connection con) {
		if(amount > balance) return false;
		boolean b = true;
		
		try {
			MySQLUtil.updateDouble(Bank.TABLE_ACCOUNTS, Bank.COLUMN_BALANCE, BankUtil.round((double) (balance - amount), 2), Bank.COLUMN_ACCOUNT_ID, String.valueOf(id), con);
		} catch (SQLException e) {
			e.printStackTrace();
			b = false;
		}
		
		if(b) balance = BankUtil.round(balance - amount, 2);
		if(getUser() != null) {
			getUser().sendScoreboard();
			getUser().updateAccountInventorys();
		}
		return b;
	}
	
	public void addTransaction(Transaction transaction, Connection con) throws SQLException {
		transactionAmount++;
		MySQLUtil.updateInt(Bank.TABLE_ACCOUNTS, Bank.COLUMN_TRANSACTIONS, transactionAmount, Bank.COLUMN_ACCOUNT_ID, id, con);
		con.prepareStatement("INSERT INTO `" + Bank.TABLE_TRANSACTIONS + "`(`" + Bank.COLUMN_SENDER_ID + "`, `" + Bank.COLUMN_TARGET_ID + "`, `" + Bank.COLUMN_VALUE + "`) VALUES ('" + transaction.getSenderId() + "', '" + transaction.getTargetId() + "', '" + transaction.getValue() + "')").execute();
		getUser().updateAccountInventorys();
	}
	
	public Collection<Transaction> getLast10SentTransaction(Connection con) throws SQLException {
		List<Transaction> list = new ArrayList<Transaction>();
		ResultSet rs = con.prepareStatement("SELECT * FROM `" + Bank.TABLE_TRANSACTIONS + "` WHERE `" + Bank.COLUMN_SENDER_ID + "` = '" + id + "' ORDER BY `" + Bank.COLUMN_ID + "` DESC LIMIT 10").executeQuery();
		while(rs.next()) {
			list.add(new Transaction(rs.getInt(Bank.COLUMN_SENDER_ID),
					rs.getInt(Bank.COLUMN_TARGET_ID),
					rs.getDouble(Bank.COLUMN_VALUE)));
		}
		
		return list;
	}
	
	public Collection<Transaction> getLast10ReveivedTransaction(Connection con) throws SQLException {
		List<Transaction> list = new ArrayList<Transaction>();
		ResultSet rs = con.prepareStatement("SELECT * FROM `" + Bank.TABLE_TRANSACTIONS + "` WHERE `" + Bank.COLUMN_TARGET_ID + "` = '" + id + "' ORDER BY `" + Bank.COLUMN_ID + "` DESC LIMIT 10").executeQuery();
		while(rs.next()) {
			list.add(new Transaction(rs.getInt(Bank.COLUMN_SENDER_ID),
					rs.getInt(Bank.COLUMN_TARGET_ID),
					rs.getDouble(Bank.COLUMN_VALUE)));
		}
		
		return list;
	}
	
	//	Static Methods
	
	public static int generateId(Connection con) throws SQLException {
		ResultSet rs = con.prepareStatement("SELECT `" + Bank.COLUMN_ACCOUNT_ID + "` FROM `" + Bank.TABLE_ACCOUNTS + "` ORDER BY `" + Bank.COLUMN_ACCOUNT_ID + "` DESC LIMIT 1").executeQuery();
		while(rs.next()) {
			return rs.getInt(Bank.COLUMN_ACCOUNT_ID) + 1;
		}
		
		return 1;
	}
	
	public static Account getNewAccount(UUID owner, Connection con) throws SQLException {
		int id = generateId(con);
		con.prepareStatement("INSERT INTO `" + Bank.TABLE_ACCOUNTS + "`(`" + Bank.COLUMN_ACCOUNT_ID + "`, `" + Bank.COLUMN_OWNER + "`) VALUES ('" + String.valueOf(id) + "', '" + owner.toString() + "')").execute();
		return new Account(id, con);
	}

}
