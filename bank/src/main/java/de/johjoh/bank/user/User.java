package de.johjoh.bank.user;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import de.johjoh.bank.Bank;
import de.johjoh.bank.sql.MySQLConnectionUtils;
import de.johjoh.bank.sql.MySQLUtil;
import de.johjoh.bank.util.BankUtil;

public class User {
	
	private UUID uniqueId;
	private double cash;
	private HashMap<Integer, Account> accounts = new HashMap<Integer, Account>();
	private HashMap<Integer, Inventory> invs = new HashMap<Integer, Inventory>();
	
	public User(UUID uniqueId, Connection con) {
		//	Load User from Database
		this.uniqueId = uniqueId;
		try {
			
			ResultSet rs = MySQLUtil.selectAll(Bank.TABLE_USER, Bank.COLUMN_UUID, uniqueId.toString(), con);
			String accountsInString = null;
			while(rs.next()) {
				cash = rs.getDouble(Bank.COLUMN_CASH);
				accountsInString = rs.getString(Bank.COLUMN_ACCOUNTS);
				if(accountsInString != null) {
					for(String id : accountsInString.split(",")) {
						Account acc = new Account(Integer.valueOf(id), con);
						accounts.put(acc.getId(), acc);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		updateAccountInventorys();
	}
	
	public UUID getUniqueId() { return uniqueId; }
	public double getCash() { return cash; }
	public Player getPlayer() { return Bukkit.getPlayer(uniqueId); }
	public HashMap<Integer, Account> getAccounts() { return accounts; }
	public Inventory getAccountInventory(int page) { return invs.get(page); }
	
	//	Add Cash with an own MySQL-Connection
	public boolean addCash(double amount) {
		Connection con = null;
		
		try {
			con = MySQLConnectionUtils.getNewConnection();
			
			boolean b = addCash(amount, con);
			MySQLConnectionUtils.closeConnection(con);
			return b;
		} catch (SQLException e) {
			e.printStackTrace();
			MySQLConnectionUtils.closeConnection(con);
			return false;
		}
	}
	
	//	Add Cash with an seperate MySQL-Connection
	public boolean addCash(double amount, Connection con) {
		boolean b = true;
		
		try {
			MySQLUtil.updateDouble(Bank.TABLE_USER, Bank.COLUMN_CASH, BankUtil.round((double) (cash + amount), 2), Bank.COLUMN_UUID, uniqueId.toString(), con);
		} catch (SQLException e) {
			e.printStackTrace();
			b = false;
		}
		
		if(b) cash = BankUtil.round(cash + amount, 2);
		sendScoreboard();
		return b;
	}

	//	Remove Cash with an own MySQL-Connection
	public boolean removeCash(double amount) {
		if(amount > cash) return false;
		Connection con = null;
		
		try {
			con = MySQLConnectionUtils.getNewConnection();
			
			boolean b = removeCash(amount, con);
			MySQLConnectionUtils.closeConnection(con);
			return b;
		} catch (SQLException e) {
			e.printStackTrace();
			MySQLConnectionUtils.closeConnection(con);
			return false;
		}
	}

	//	Add Cash with an seperate MySQL-Connection
	public boolean removeCash(double amount, Connection con) {
		if(amount > cash) return false;
		boolean b = true;
		
		try {
			MySQLUtil.updateDouble(Bank.TABLE_USER, Bank.COLUMN_CASH, BankUtil.round((double) (cash - amount), 2), Bank.COLUMN_UUID, uniqueId.toString(), con);
		} catch (SQLException e) {
			e.printStackTrace();
			b = false;
		}
		
		if(b) cash = BankUtil.round(cash - amount, 2);
		sendScoreboard();
		return b;
	}
	
	//	Add new Account for the User incl. Database
	public Account createNewAccount() {
		Connection con = null;
		Account account = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			account = Account.getNewAccount(uniqueId, con);
			String s = "";
			for(Account acc : accounts.values()) {
				s += acc.getId() + ",";
			}
			s += account.getId();
			MySQLUtil.updateString(Bank.TABLE_USER, Bank.COLUMN_ACCOUNTS, s, Bank.COLUMN_UUID, uniqueId.toString(), con);
			accounts.put(account.getId(), account);
			sendScoreboard();
		} catch (SQLException e) {
			MySQLConnectionUtils.closeConnection(con);
			return null;
		}
		MySQLConnectionUtils.closeConnection(con);
		updateAccountInventorys();
		return account;
	}
	
	//	Pay all Interests for the User's Accounts
	public double payInterests(Connection con) {
		double all = 0.0D;
		for(Account account : accounts.values()) {
			double d = account.getBalance() * Bank.getInstance().getZinsen();
			d = BankUtil.round(d, 2);
			account.addBalance(d, con);
			all+=d;
		}
		sendScoreboard();
		updateAccountInventorys();
		return BankUtil.round(all, 2);
	}
	
	//	Returns his whole Money on his Account
	public double getWholeMoney() {
		double d = 0.0D;
		for(Account account : accounts.values()) {
			d+=account.getBalance();
		}
		return BankUtil.round(d, 2);
	}
	
	//	Returns Account of an User by the Account-ID
	public Account getAccount(String id) {
		for(Account account : accounts.values()) {
			if(account.getIdToString().equalsIgnoreCase(id)) {
				return accounts.get(BankUtil.idFromString(id));
			}
		}
		return null;
	}
	
	//	Removes Account from User and Database
	public boolean removeAccount(Account account) {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			String s = "";
			for(Account acc : accounts.values()) {
				if(acc.getId() == account.getId()) continue;
				s += acc.getId() + ",";
			}
			MySQLUtil.updateString(Bank.TABLE_USER, Bank.COLUMN_ACCOUNTS, s, Bank.COLUMN_UUID, uniqueId.toString(), con);
			accounts.remove(account.getId());
			con.prepareStatement("DELETE FROM `" + Bank.TABLE_ACCOUNTS + "` WHERE `" + Bank.COLUMN_ACCOUNT_ID + "` = '" + account.getId() + "'").execute();
			con.prepareStatement("DELETE FROM `" + Bank.TABLE_TRANSACTIONS + "` WHERE `" + Bank.COLUMN_SENDER_ID + "` = '" + account.getId() + "'").execute();
			con.prepareStatement("DELETE FROM `" + Bank.TABLE_TRANSACTIONS + "` WHERE `" + Bank.COLUMN_TARGET_ID + "` = '" + account.getId() + "'").execute();
			addCash(account.getBalance(), con);
		} catch (SQLException e) {
			MySQLConnectionUtils.closeConnection(con);
			return false;
		}
		
		sendScoreboard();
		updateAccountInventorys();
		MySQLConnectionUtils.closeConnection(con);
		return true;
	}
	
	public void updateAccountInventorys() {
		int i = 0;
		Inventory in = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Konten");
		invs.put(i, in);
		for(Account account : accounts.values()) {
			
			ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, (byte) (account.getBalance() > 0 ? 13 : 14));
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.AQUA + "" + account.getIdToString());
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Kontostand" + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + account.getBalance());
			lore.add(ChatColor.GRAY + "Transaktionen" + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + account.getTranactionsAmount());
			meta.setLore(lore);
			item.setItemMeta(meta);
			in.addItem(item);
			
			if(in.getItem(52) != null) {
				in.addItem(BankUtil.nextItem(i+2));
				in = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Konten");
				i++;
				invs.put(i, in);
			}
		}
	}
	
	public void sendScoreboard() {
		Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective obj = sb.getObjective("aaa") != null ? sb.getObjective("aaa") : sb.registerNewObjective("aaa", "bbb");
		
		obj.setDisplayName(ChatColor.GOLD + "Bank");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		obj.getScore(ChatColor.GRAY + "Bargeld" + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + cash).setScore(2);
		obj.getScore(ChatColor.GRAY + "Konten" + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + accounts.size()).setScore(1);
		obj.getScore(ChatColor.GRAY + "Gesamtvermögen" + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + getWholeMoney()).setScore(0);
		
		getPlayer().setScoreboard(sb);
	}

}
