package de.johjoh.bank.user;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import de.johjoh.bank.util.BankUtil;

public class UserStorage {
	
	public static HashMap<UUID, User> users = new HashMap<UUID, User>();
	
	public static void registerUser(User user) { users.put(user.getUniqueId(), user); }
	public static void unregisterUser(UUID uniqueId) { users.remove(uniqueId); }
	public static User getUser(UUID uniqueId) { return users.get(uniqueId); }
	public static Collection<User> getUsers() { return BankUtil.copy(users.values()); }

}
