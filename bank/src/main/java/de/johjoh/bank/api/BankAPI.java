package de.johjoh.bank.api;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.Bukkit;

import de.johjoh.bank.Bank;
import de.johjoh.bank.sql.MySQLConnectionUtils;
import de.johjoh.bank.sql.MySQLUtil;
import de.johjoh.bank.user.Account;
import de.johjoh.bank.user.User;
import de.johjoh.bank.user.UserStorage;
import de.johjoh.bank.util.BankUtil;

public class BankAPI {
	
	/**
	 * @param uniqueId Ziel-Spieler
	 * @throws SQLException
	 * @return double: Bargeld
	 */
	public static double getCash(UUID uniqueId) throws SQLException {
		if(Bukkit.getPlayer(uniqueId) != null) {
			return UserStorage.getUser(uniqueId).getCash();
		}
		double d = 0.0D;
		Connection con = MySQLConnectionUtils.getNewConnection();
		ResultSet rs = MySQLUtil.select(Bank.COLUMN_CASH, Bank.TABLE_USER, Bank.COLUMN_UUID, uniqueId.toString(), con);
		while(rs.next()) {
			d = rs.getDouble(Bank.COLUMN_CASH);
		}
		MySQLConnectionUtils.closeConnection(con);
		return d;
	}
	
	/**
	 * @param uniqueId Ziel-Spieler
	 * @throws SQLException
	 * @return double: Kontostand
	 */
	public static double getAccountBalance(String accountId) throws SQLException {
		double balance = 0.0D;
		Connection con = null;
		con = MySQLConnectionUtils.getNewConnection();
		Account acc = new Account(BankUtil.idFromString(accountId), con);
		if(acc.getOwner() != null) balance = acc.getBalance();
		
		MySQLConnectionUtils.closeConnection(con);
		return balance;
	}
	
	/**
	 * @param amount Menge des zu überweisenden Geldes
	 * @param uniqueId Ziel-Spieler
	 * @return boolean: success
	 */
	public static boolean addCash(double amount, UUID uniqueId) {
		User user = UserStorage.getUser(uniqueId);
		if(user.addCash(amount)) return true;
		return false;
	}
	
	/**
	 * @param amount Menge des zu abziehenden Geldes
	 * @param uniqueId Ziel-Spieler
	 * @return boolean: success
	 */
	public static boolean removeCash(double amount, UUID uniqueId) {
		User user = UserStorage.getUser(uniqueId);
		if(user.removeCash(amount)) return true;
		return false;
	}
	
	/**
	 * @param amount Menge des zu überweisenden Geldes
	 * @param accountId Ziel-Kontonummer
	 * @throws SQLException
	 * @return boolean: success
	 */
	public static boolean addMoneyToAccount(double amount, String accountId) throws SQLException {
		Connection con = null;
		con = MySQLConnectionUtils.getNewConnection();
		Account acc = new Account(BankUtil.idFromString(accountId), con);
		if(acc.getOwner() == null) { MySQLConnectionUtils.closeConnection(con); return false; }
		if(Bukkit.getPlayer(acc.getOwner()) != null) {
			User user = UserStorage.getUser(acc.getOwner());
			if(!user.getAccount(accountId).addBalance(amount, con)) { MySQLConnectionUtils.closeConnection(con); return false; }
		}
		else {
			if(!acc.addBalance(amount, con)) { MySQLConnectionUtils.closeConnection(con); return false; }
			
		}
		MySQLConnectionUtils.closeConnection(con);
		return true;
	}
	
	/**
	 * @param amount Menge des zu abziehenden Geldes
	 * @param accountId Ziel-Kontonummer
	 * @throws SQLException
	 * @return boolean: success
	 */
	public static boolean removeMoneyFromAccount(double amount, String accountId) throws SQLException {
		Connection con = null;
		con = MySQLConnectionUtils.getNewConnection();
		Account acc = new Account(BankUtil.idFromString(accountId), con);
		if(acc.getOwner() == null) { MySQLConnectionUtils.closeConnection(con); return false; }
		if(acc.getBalance() < amount) { MySQLConnectionUtils.closeConnection(con); return false; }
		if(Bukkit.getPlayer(acc.getOwner()) != null) {
			User user = UserStorage.getUser(acc.getOwner());
			if(!user.getAccount(accountId).removeBalance(amount, con)) { MySQLConnectionUtils.closeConnection(con); return false; }
		}
		else {
			if(!acc.removeBalance(amount, con)) { MySQLConnectionUtils.closeConnection(con); return false; }
			
		}
		MySQLConnectionUtils.closeConnection(con);
		return true;
	}

}
