package de.johjoh.bank.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import de.johjoh.bank.Bank;

public class MySQLConnectionUtils {
	
	private static String host;
	private static int port = 3306;
	private static String username;
	private static String password;
	private static String database;
	
	//	Loads MySQL-Values from config.yml
	public static boolean reloadValuesFromConfig() {

		host = Bank.getInstance().getConfig().getString("mysql.host");
		port = Bank.getInstance().getConfig().getInt("mysql.port");
		username = Bank.getInstance().getConfig().getString("mysql.username");
		password = Bank.getInstance().getConfig().getString("mysql.password");
		database = Bank.getInstance().getConfig().getString("mysql.database");
		
		return host != null && username != null && password != null && database != null;
	}
	
	//	Creates an new MySQL-Connection
	public static Connection getNewConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
	}
	
	//	Checks the Connection Errors
	public static boolean isConnectionValid(Connection con) throws SQLException {
		if(con != null) {
			if(!con.isClosed()) {
				if(con.isValid(1)) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	//	Closes an existing MySQL-Connection
	public static boolean closeConnection(Connection con) {
		try {
			if(con != null) {
				con.close();
			}
			con = null;
			return true;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

}
