package de.johjoh.bank.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.johjoh.bank.Bank;

public class MySQLUtil {
	
	public static void updateInt(String table, String col, int val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateInt(String table, String col, int val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static void updateDouble(String table, String col, double val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateDouble(String table, String col, double val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static void updateString(String table, String col, String val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`='" + val + "' WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateString(String table, String col, String val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`='" + val + "' WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static void updateBoolean(String table, String col, boolean val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + (val ? 1 : 0) + " WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateBoolean(String table, String col, boolean val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + (val ? 1 : 0) + " WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static ResultSet select(String what, String table, String where, String whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT `" + what + "` FROM `" + table + "` WHERE `" + where + "`='" + whereVal + "'").executeQuery();
	}
	
	public static ResultSet select(String what, String table, String where, int whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT `" + what + "` FROM `" + table + "` WHERE `" + where + "`=" + whereVal + "").executeQuery();
	}
	
	public static ResultSet selectAll(String table, String where, String whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT * FROM `" + table + "` WHERE `" + where + "`='" + whereVal + "'").executeQuery();
	}
	
	public static ResultSet selectAll(String table, String where, int whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT * FROM `" + table + "` WHERE `" + where + "`=" + whereVal + "").executeQuery();
	}
	
	//	Creates User, Account, Transaction Table
	public static void createTables(Connection con) throws SQLException {
		con.prepareStatement("CREATE TABLE IF NOT EXISTS `" + Bank.TABLE_USER + "`(`" + Bank.COLUMN_ID + "` INT(10) AUTO_INCREMENT NOT NULL, `" + Bank.COLUMN_UUID + "` VARCHAR(36) NOT NULL, `" + Bank.COLUMN_NAME + "` VARCHAR(16) NOT NULL, `" + Bank.COLUMN_CASH + "` DOUBLE NOT NULL DEFAULT '0.0', `" + Bank.COLUMN_ACCOUNTS + "` TEXT NULL DEFAULT NULL, PRIMARY KEY (`" + Bank.COLUMN_ID + "`))").execute();
		con.prepareStatement("CREATE TABLE IF NOT EXISTS `" + Bank.TABLE_ACCOUNTS + "`(`" + Bank.COLUMN_ID + "` INT(10) AUTO_INCREMENT NOT NULL, `" + Bank.COLUMN_ACCOUNT_ID + "` INT(6) NOT NULL, `" + Bank.COLUMN_OWNER + "` VARCHAR(36) NOT NULL, `" + Bank.COLUMN_BALANCE + "` DOUBLE NOT NULL DEFAULT '0.0', `" + Bank.COLUMN_TRANSACTIONS + "` INT NOT NULL DEFAULT 0, PRIMARY KEY (`" + Bank.COLUMN_ID + "`))").execute();
		con.prepareStatement("CREATE TABLE IF NOT EXISTS `" + Bank.TABLE_TRANSACTIONS + "`(`" + Bank.COLUMN_ID + "` INT(10) AUTO_INCREMENT NOT NULL, `" + Bank.COLUMN_SENDER_ID + "` INT(6) NOT NULL, `" + Bank.COLUMN_TARGET_ID + "` INT(6) NOT NULL, `" + Bank.COLUMN_VALUE + "` DOUBLE NOT NULL, PRIMARY KEY (`" + Bank.COLUMN_ID + "`))").execute();
	}

}
