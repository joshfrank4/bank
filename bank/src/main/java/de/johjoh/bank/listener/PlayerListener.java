package de.johjoh.bank.listener;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import de.johjoh.bank.Bank;
import de.johjoh.bank.user.User;
import de.johjoh.bank.user.UserStorage;

public class PlayerListener implements Listener {
	
	private ArrayList<Material> meat = new ArrayList<Material>();
	private ArrayList<EntityType> monsters = new ArrayList<EntityType>();
	
	public PlayerListener() {
		Bukkit.getPluginManager().registerEvents(this, Bank.getInstance());
		
		meat.add(Material.PORK);
		meat.add(Material.GRILLED_PORK);
		meat.add(Material.RAW_BEEF);
		meat.add(Material.COOKED_BEEF);
		meat.add(Material.RAW_CHICKEN);
		meat.add(Material.COOKED_CHICKEN);
		meat.add(Material.RABBIT);
		meat.add(Material.COOKED_RABBIT);
		meat.add(Material.MUTTON);
		meat.add(Material.COOKED_MUTTON);
		meat.add(Material.ROTTEN_FLESH);

		monsters.add(EntityType.CREEPER);
		monsters.add(EntityType.SKELETON);
		monsters.add(EntityType.SPIDER);
		monsters.add(EntityType.CAVE_SPIDER);
		monsters.add(EntityType.ZOMBIE);
		monsters.add(EntityType.SLIME);
		monsters.add(EntityType.GHAST);
		monsters.add(EntityType.PIG_ZOMBIE);
		monsters.add(EntityType.ENDERMAN);
		monsters.add(EntityType.SILVERFISH);
		monsters.add(EntityType.WITHER);
		monsters.add(EntityType.ENDER_DRAGON);
		monsters.add(EntityType.ENDER_CRYSTAL);
		monsters.add(EntityType.BLAZE);
		monsters.add(EntityType.MAGMA_CUBE);
		monsters.add(EntityType.WITCH);
		monsters.add(EntityType.ENDERMITE);
		monsters.add(EntityType.GUARDIAN);
	}
	
	@EventHandler
	public void onPlayerItemConsume(PlayerItemConsumeEvent e) {
		//	Checking wether Player consumes Meat
		if(meat.contains(e.getItem().getType()) && !e.getItem().getType().equals(Material.ROTTEN_FLESH)) {
			User user = UserStorage.getUser(e.getPlayer().getUniqueId());
			if(user.getCash() >= Bank.getInstance().getEatMeatCosts()) {
				//	Removing Cash from User and Database
				user.removeCash(Bank.getInstance().getEatMeatCosts());
				e.getPlayer().playSound(e.getPlayer().getEyeLocation(), Sound.BLAZE_HIT, 1.0F, 1.0F);
				e.getPlayer().sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast Fleisch gegessen, dir wurden " + ChatColor.YELLOW + Bank.getInstance().getEatMeatCosts() + "�" + ChatColor.RED + " abgezogen!");
			}
			else {
				e.setCancelled(true);
				e.getPlayer().playSound(e.getPlayer().getEyeLocation(), Sound.BLAZE_HIT, 1.0F, 1.0F);
				e.getPlayer().sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast nicht gen�gend Geld um Fleisch zu essen!");
			}
		}
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		if(e.getEntity().getKiller() == null || !(e.getEntity().getKiller() instanceof Player)) return;
		User user = UserStorage.getUser(e.getEntity().getKiller().getUniqueId());
		if(monsters.contains(e.getEntity().getType())) {
			//	Give Cash-Reward for Monster-Kill
			user.addCash(Bank.getInstance().getMonsterKillReward());
			e.getEntity().getKiller().playSound(e.getEntity().getKiller().getEyeLocation(), Sound.NOTE_PLING, 1.0F, 1.0F);
			e.getEntity().getKiller().sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast ein Monster get�tet und dadurch " + ChatColor.YELLOW + Bank.getInstance().getMonsterKillReward() + "�" + ChatColor.GREEN + " erhalten!");
		}
		else if(!monsters.contains(e.getEntity().getType()) && !e.getEntity().getType().equals(EntityType.VILLAGER)) {
			
			//	Checking wether User has enough Money
			if(user.getCash() >= Bank.getInstance().getAnimalKillCosts()) {
				//	Removing Money from User-Cash
				user.removeCash(Bank.getInstance().getAnimalKillCosts());
				e.getEntity().getKiller().playSound(e.getEntity().getKiller().getEyeLocation(), Sound.BLAZE_HIT, 1.0F, 1.0F);
				e.getEntity().getKiller().sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast ein Tier get�tet, deshalb wurden dir " + ChatColor.YELLOW + Bank.getInstance().getAnimalKillCosts() + "�" + ChatColor.RED + " abgezogen!");
			}
			else {
				e.getEntity().getKiller().playSound(e.getEntity().getKiller().getEyeLocation(), Sound.BLAZE_HIT, 1.0F, 1.0F);
				e.getEntity().getKiller().sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast ein Tier get�tet, jedoch konnte dir kein Geld abgezogen werden, da du keins mehr besitzt!");
				e.getEntity().getKiller().sendMessage(Bank.PREFIX + ChatColor.RED + "Anzeige ist raus!");
			}
		}
	}
	
	@EventHandler
	public void onPlayerInteractAtEntity(final PlayerInteractAtEntityEvent e) {
		//	Checking wether Player fed animal
		if(monsters.contains(e.getRightClicked().getType()) || e.getRightClicked().getType().equals(EntityType.VILLAGER)) return;
		if(e.getPlayer().getItemInHand() == null || e.getPlayer().getItemInHand().getType().equals(Material.SNOW_BALL) ||
				e.getPlayer().getItemInHand().getType().equals(Material.ENDER_PEARL) || e.getPlayer().getItemInHand().getType().equals(Material.EYE_OF_ENDER) ||
				e.getPlayer().getItemInHand().getType().equals(Material.EGG) || e.getPlayer().getItemInHand().getType().equals(Material.FIREBALL) ||
				e.getPlayer().getItemInHand().getType().equals(Material.FIREWORK) || e.getPlayer().getItemInHand().getType().equals(Material.POTION)) return;
		
		final int a = e.getPlayer().getItemInHand().getAmount();
		Bukkit.getScheduler().runTaskLater(Bank.getInstance(), new Runnable() {
			
			public void run() {
				if(e.getPlayer().getItemInHand().getAmount() < a) {
					User user = UserStorage.getUser(e.getPlayer().getUniqueId());
					//	Adding Money to User and Database
					user.addCash(Bank.getInstance().getAnimalFeedReward());
					e.getPlayer().playSound(e.getPlayer().getEyeLocation(), Sound.NOTE_PLING, 1.0F, 1.0F);
					e.getPlayer().sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast ein Tier gef�ttert, deshalb hast du " + ChatColor.YELLOW + Bank.getInstance().getAnimalFeedReward() + "�" + ChatColor.GREEN + " erhalten!");
					
				}
			}
		}, 10L);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if(e.getClickedBlock().getType().equals(Material.GOLD_BLOCK)) {
				//	Opening Account-Inventory for Player
				e.getPlayer().openInventory(UserStorage.getUser(e.getPlayer().getUniqueId()).getAccountInventory(0));
			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getInventory().getTitle().equalsIgnoreCase(ChatColor.GOLD + "Konten")) {
			e.setCancelled(true);
			if(e.getCurrentItem() != null) {
				if(e.getCurrentItem().hasItemMeta()) {
					if(e.getCurrentItem().getItemMeta().getDisplayName().contains(ChatColor.GREEN + "Seite ")) {
						User u = UserStorage.getUser(e.getWhoClicked().getUniqueId());
						//	Opening next Page of Account-Inventory
						try {
							u.getPlayer().openInventory(u.getAccountInventory(Integer.valueOf(e.getCurrentItem().getItemMeta().getDisplayName().split(ChatColor.GREEN + "Seite ")[1])-1));
						} catch(NumberFormatException ex) {
							u.getPlayer().openInventory(u.getAccountInventory(0));
						}
					}
				}
			}
		}
	}

}
