package de.johjoh.bank.listener;

import java.sql.Connection;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.johjoh.bank.Bank;
import de.johjoh.bank.sql.MySQLConnectionUtils;
import de.johjoh.bank.user.User;
import de.johjoh.bank.user.UserStorage;
import de.johjoh.bank.util.BankUtil;

public class JoinQuitListener implements Listener {
	
	public JoinQuitListener() {
		Bukkit.getPluginManager().registerEvents(this, Bank.getInstance());
	}
	
	@EventHandler
	public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent e) {
		//	Insert Player into Database
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			
			BankUtil.registerPlayer(e.getUniqueId(), e.getName(), con);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		MySQLConnectionUtils.closeConnection(con);
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		//	Add Player into UserStorage
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		UserStorage.registerUser(new User(e.getPlayer().getUniqueId(), con));
		UserStorage.getUser(e.getPlayer().getUniqueId()).sendScoreboard();
		
		MySQLConnectionUtils.closeConnection(con);
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		//	Removing Player from Storage
		UserStorage.unregisterUser(e.getPlayer().getUniqueId());
	}

}
