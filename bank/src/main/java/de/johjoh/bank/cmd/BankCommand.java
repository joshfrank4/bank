package de.johjoh.bank.cmd;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.johjoh.bank.Bank;
import de.johjoh.bank.sql.MySQLConnectionUtils;
import de.johjoh.bank.user.Account;
import de.johjoh.bank.user.Transaction;
import de.johjoh.bank.user.User;
import de.johjoh.bank.user.UserStorage;
import de.johjoh.bank.util.BankUtil;

public class BankCommand implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player)sender;
			if(args.length == 0 || (args.length >= 1 && args[0].equalsIgnoreCase("help"))) {
				
				sendHelp(player);
				
			}
			else if(args.length >= 1) {
				if(args[0].equalsIgnoreCase("balance")) {
					
					//	Checking for Permissions
					if(!Bank.getInstance().getPermission().playerHas(player, "bank.balance")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
					
					//	Loading User and send Cash-Amount
					User u = UserStorage.getUser(player.getUniqueId());
					if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
					player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Dein Bargeld" + ChatColor.GRAY + ": " + ChatColor.YELLOW + u.getCash());
					
				}
				else if(args[0].equalsIgnoreCase("pay")) {
					
					//	Checking for Permissions
					if(!Bank.getInstance().getPermission().playerHas(player, "bank.pay")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
					
					if(args.length <= 2) {
						player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank pay <Spieler> <Anzahl>");
					}
					else {
						if(Bukkit.getPlayer(args[1]) != null) {
							
							Player player2 = Bukkit.getPlayer(args[1]);
							
							if(player == player2) {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du kannst kein Geld an dich selber zahlen!");
								return true;
							}
							double amount = 0.0D;
							
							//	Loading amount from args[2]
							try {
								amount = Double.valueOf(args[2]);
								amount = BankUtil.round(amount, 2);
							} catch(NumberFormatException e) {
								player.sendMessage(Bank.PREFIX + ChatColor.YELLOW + args[2] + ChatColor.RED + " muss eine Zahl sein!");
								return true;
							}
							
							//	Loading Users
							User u1 = UserStorage.getUser(player.getUniqueId());
							if(u1 == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
							User u2 = UserStorage.getUser(player2.getUniqueId());
							if(u2 == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
							
							if(u1.getCash() >= amount) {
								Connection con = null;
								
								try {
									con = MySQLConnectionUtils.getNewConnection();
								} catch (SQLException e) {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
									MySQLConnectionUtils.closeConnection(con);
									return true;
								}
								
								//	Transfering Money
								if(u1.removeCash(amount, con) && u2.addCash(amount, con)) {
									player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast " + ChatColor.GOLD + player2.getDisplayName() + " " + ChatColor.YELLOW + amount + "€" + ChatColor.GREEN + " bezahlt.");
									player2.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast " + ChatColor.YELLOW + amount + "€" + ChatColor.GREEN + " von " + ChatColor.GOLD + player.getDisplayName() + ChatColor.GREEN + " erhalten.");
								}
								else {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
								}
								
								MySQLConnectionUtils.closeConnection(con);
							}
							else {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast nicht genügend Geld!");
								return true;
							}
						}
						else {
							player.sendMessage(Bank.PREFIX + ChatColor.RED + "Dieser Spieler ist nicht online!");
						}
					}
					
				}
				else if(args[0].equalsIgnoreCase("account")) {
					
					if(args.length >= 2) {
						
						if(args[1].equalsIgnoreCase("create")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.create")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							//	Creating new Account (for Player and Database)
							User u = UserStorage.getUser(player.getUniqueId());
							if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
							Account acc = u.createNewAccount();
							if(acc == null) {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
								return true;
							}
							
							player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast einen neues Konto erstellt.");
							player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Kontonummer" + ChatColor.GRAY + ": " + ChatColor.YELLOW + acc.getIdToString());
							return true;
							
						}
						else if(args[1].equalsIgnoreCase("delete")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.delete")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							if(args.length >= 3) {
								User u = UserStorage.getUser(player.getUniqueId());
								if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
								Account acc = u.getAccount(args[2]);
								
								if(acc != null) {
									//	Deleting Account from User and Database
									if(u.removeAccount(acc)) {
										player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast dein Konto, mit der Kontonummer " + ChatColor.YELLOW + acc.getIdToString() + ChatColor.GREEN + " gelöscht.");
										player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Das Geld darauf wurde zu deinem Bargeld hinzugefügt.");
									}
									else {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
										return true;
									}
									
								}
								else {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt kein Konto mit der Kontonummer " + ChatColor.YELLOW + args[2]);
								}
							}
							else {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank account delete <Kontonummer>");
							}
							
						}
						else if(args[1].equalsIgnoreCase("deposit")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.deposit")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							if(args.length >= 4) {
								User u = UserStorage.getUser(player.getUniqueId());
								if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
								Account acc = u.getAccount(args[2]);
								if(acc != null) {
									double amount = 0.0D;
									
									//	Loading amount from args[3]
									try {
										amount = Double.valueOf(args[3]);
										amount = BankUtil.round(amount, 2);
									} catch(NumberFormatException e) {
										player.sendMessage(Bank.PREFIX + ChatColor.YELLOW + args[3] + ChatColor.RED + " muss eine Zahl sein!");
										return true;
									}
									
									//	Checking wether User has enough money
									if(u.getCash() < amount) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast nicht genügend Geld!");
										return true;
									}
									
									Connection con = null;
									
									try {
										con = MySQLConnectionUtils.getNewConnection();
									} catch (SQLException e) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
										MySQLConnectionUtils.closeConnection(con);
										return true;
									}
									
									//	Transfering Money
									if(!(u.removeCash(amount, con) && acc.addBalance(amount, con))) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
										MySQLConnectionUtils.closeConnection(con);
										return true;
									}
									
									player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast " + ChatColor.YELLOW + amount + "€" + ChatColor.GREEN + " auf dein Konto eingezahlt.");
									
									MySQLConnectionUtils.closeConnection(con);
									return true;
								}
								else {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt kein Konto mit der Kontonummer " + ChatColor.YELLOW + args[2]);
								}
							}
							else {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank account deposit <Kontonummer> <Menge>");
							}
							
						}
						else if(args[1].equalsIgnoreCase("payoff")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.payoff")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							if(args.length >= 4) {
								User u = UserStorage.getUser(player.getUniqueId());
								if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
								Account acc = u.getAccount(args[2]);
								
								if(acc != null) {
									double amount = 0.0D;
									
									//	Loading amount from args[3]
									try {
										amount = Double.valueOf(args[3]);
										amount = BankUtil.round(amount, 2);
									} catch(NumberFormatException e) {
										player.sendMessage(Bank.PREFIX + ChatColor.YELLOW + args[3] + ChatColor.RED + " muss eine Zahl sein!");
										return true;
									}
									
									//	Checking wether Account has enough Balance
									if(acc.getBalance() < amount) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast nicht genügend Geld auf diesem Konto!");
										return true;
									}
									
									Connection con = null;
									
									try {
										con = MySQLConnectionUtils.getNewConnection();
									} catch (SQLException e) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
										MySQLConnectionUtils.closeConnection(con);
										return true;
									}
									
									//	Transfering Money from Account to Player-Cash
									if(!(acc.removeBalance(amount, con) && u.addCash(amount, con))) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
										MySQLConnectionUtils.closeConnection(con);
										return true;
									}
									
									player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast " + ChatColor.YELLOW + amount + "€" + ChatColor.GREEN + " von deinem Konto abgehoben.");
									MySQLConnectionUtils.closeConnection(con);
									return true;
								}
								else {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt kein Konto mit der Kontonummer " + ChatColor.YELLOW + args[2]);
								}
							}
							else {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank account deposit <Kontonummer> <Menge>");
							}
							
						}
						else if(args[1].equalsIgnoreCase("balance")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.balance ")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							if(args.length >= 3) {
								User u = UserStorage.getUser(player.getUniqueId());
								if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
								Account acc = u.getAccount(args[2]);
								
								//	Checking wether User has Account
								if(acc != null) {
									//	Send Player Balance
									player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Dein aktueller Kontostand von " + ChatColor.YELLOW + acc.getIdToString() + ChatColor.GREEN + " beträgt" + ChatColor.GRAY + ": " + ChatColor.YELLOW + acc.getBalance());
								}
								else {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt kein Konto mit der Kontonummer " + ChatColor.YELLOW + args[2]);
								}
								
							}
							else {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank account delete <Kontonummer>");
							}
							
						}
						else if(args[1].equalsIgnoreCase("list")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.list ")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							User u = UserStorage.getUser(player.getUniqueId());
							if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
							
							//	Checking wether User has Accounts
							if(u.getAccounts().size() == 0) {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt keine Konten!");
								return true;
							}
							
							//	Listing Accounts of User
							player.sendMessage(Bank.PREFIX + ChatColor.GOLD + "Deine Konten" + ChatColor.DARK_GRAY + ":");
							for(Account acc : u.getAccounts().values()) {
								player.sendMessage(ChatColor.GRAY + "- " + ChatColor.GREEN + acc.getIdToString() + ChatColor.GRAY + " : " + ChatColor.YELLOW + acc.getBalance());
							}
							
						}
						else if(args[1].equalsIgnoreCase("transfer")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.transfer ")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							if(args.length >= 5) {
								
								if(args[2].equalsIgnoreCase(args[3])) {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du kannst kein Geld auf das selbe Konto überweisen!");
									return true;
								}
								
								User u = UserStorage.getUser(player.getUniqueId());
								if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
								Account acc = u.getAccount(args[2]);
								
								//	Checking wether User has Account
								if(acc != null) {
									int id;
									
									try {
										id = BankUtil.idFromString(args[3]);
									} catch(NumberFormatException e) {
										player.sendMessage(Bank.PREFIX + ChatColor.YELLOW + args[3] + ChatColor.RED + " muss eine gültige Kontonummer sein.");
										return true;
									}
									
									double amount = 0.0D;
									
									//	Loading amount from args[4]
									try {
										amount = Double.valueOf(args[4]);
										amount = BankUtil.round(amount, 2);
									} catch(NumberFormatException e) {
										player.sendMessage(Bank.PREFIX + ChatColor.YELLOW + args[4] + ChatColor.RED + " muss eine Zahl sein!");
										return true;
									}
									
									//	Checking wether Account has enough Money
									if(acc.getBalance() < amount) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast nicht genügend Geld auf diesem Konto!");
										return true;
									}
									
									Connection con = null;
									
									try {
										con = MySQLConnectionUtils.getNewConnection();
									} catch (SQLException e) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
										MySQLConnectionUtils.closeConnection(con);
										return true;
									}
									
									Account acc2 = new Account(id, con);
									
									//	Checking wether Account is existing
									if(acc2.getOwner() == null) {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Das Konto " + ChatColor.YELLOW + args[3] + ChatColor.RED + " existiert nicht!");
										MySQLConnectionUtils.closeConnection(con);
										return true;
									}
									
									if(Bukkit.getPlayer(acc2.getOwner()) != null) {
										User u2 = UserStorage.getUser(acc2.getOwner());
										if(u2 == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
										
										//	Checking wether Account is existing
										if(u2.getAccount(args[3]) == null) {
											player.sendMessage(Bank.PREFIX + ChatColor.RED + "Das Konto " + ChatColor.YELLOW + args[3] + ChatColor.RED + " existiert nicht!");
											MySQLConnectionUtils.closeConnection(con);
											return true;
										}
										
										//	Transfering Money
										if(!(acc.removeBalance(amount, con) && u2.getAccount(args[3]).addBalance(amount, con))) {
											player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
											MySQLConnectionUtils.closeConnection(con);
											return true;
										}
										
										try {
											acc.addTransaction(new Transaction(acc.getId(), u2.getAccount(args[3]).getId(), amount), con);
										} catch (SQLException e) {}
										
										player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast an das Konto " + ChatColor.GOLD + acc2.getIdToString() + " " + ChatColor.YELLOW + amount + "€" + ChatColor.GREEN + " überwiesen.");
									}
									else {
										Account acc3 = new Account(BankUtil.idFromString(args[3]), con);
										
										//	Checking wether Account is existing
										if(acc3.getOwner() == null) {
											player.sendMessage(Bank.PREFIX + ChatColor.RED + "Das Konto " + ChatColor.YELLOW + args[3] + ChatColor.RED + " existiert nicht!");
											MySQLConnectionUtils.closeConnection(con);
											return true;
										}
										
										//	Transfering Money
										if(!(acc.removeBalance(amount, con) && acc3.addBalance(amount, con))) {
											player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
											MySQLConnectionUtils.closeConnection(con);
											return true;
										}
										
										//	Add Transaction to Database
										try {
											acc.addTransaction(new Transaction(acc.getId(), acc3.getId(), amount), con);
										} catch (SQLException e) {}
										
										player.sendMessage(Bank.PREFIX + ChatColor.GREEN + "Du hast an das Konto " + ChatColor.GOLD + acc2.getIdToString() + " " + ChatColor.YELLOW + amount + "€" + ChatColor.GREEN + " überwiesen.");
									}
									MySQLConnectionUtils.closeConnection(con);
								}
								else {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt kein Konto mit der Kontonummer " + ChatColor.YELLOW + args[2]);
								}
							}
							else {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank account transfer <Kontonummer> <Ziel-Kontonummer> <Menge>");
							}
							
						}
						else if(args[1].equalsIgnoreCase("transactions")) {
							
							//	Checking for Permissions
							if(!Bank.getInstance().getPermission().playerHas(player, "bank.account.transactions ")) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast keine Rechte auf diesen Befehl!"); return true; }
							
							if(args.length >= 4) {
								if(args[2].equalsIgnoreCase("sent")) {
									User u = UserStorage.getUser(player.getUniqueId());
									if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
									
									if(u.getAccount(args[3]) != null) {
										Account acc = u.getAccount(args[3]);
										Connection con = null;
										
										try {
											con = MySQLConnectionUtils.getNewConnection();
											//	Loading Transactions
											List<Transaction> tr = (List<Transaction>) acc.getLast10SentTransaction(con);
											if(tr.size() > 0) {
												player.sendMessage(Bank.PREFIX + ChatColor.GOLD + "Letzte gesendete Überweisungen" + ChatColor.DARK_GRAY + ":");
												for(Transaction tra : tr) {
													player.sendMessage(ChatColor.GRAY + "- " + ChatColor.RED + BankUtil.getIdToString(tra.getSenderId()) + ChatColor.GRAY + " --> " + ChatColor.GREEN + BankUtil.getIdToString(tra.getTargetId()) + ChatColor.GRAY + " : " + ChatColor.YELLOW + tra.getValue() + "€");
												}
											}
											else {
												player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast auf diesem Konto keine Überweisungen gesendet!");
											}
										} catch (SQLException e) {
											player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
										}
										
										MySQLConnectionUtils.closeConnection(con);
									}
									else {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt kein Konto mit der Kontonummer " + ChatColor.YELLOW + args[3]);
									}
								}
								else if(args[2].equalsIgnoreCase("received")) {
									User u = UserStorage.getUser(player.getUniqueId());
									if(u == null) { player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator."); return true; }
									
									if(u.getAccount(args[3]) != null) {
										Account acc = u.getAccount(args[3]);
										Connection con = null;
										
										try {
											con = MySQLConnectionUtils.getNewConnection();
											//	Loading Transactions
											List<Transaction> tr = (List<Transaction>) acc.getLast10ReveivedTransaction(con);
											if(tr.size() > 0) {
												player.sendMessage(Bank.PREFIX + ChatColor.GOLD + "Letzte erhaltene Überweisungen" + ChatColor.DARK_GRAY + ":");
												for(Transaction tra : tr) {
													player.sendMessage(ChatColor.GRAY + "- " + ChatColor.GREEN + BankUtil.getIdToString(tra.getSenderId()) + ChatColor.GRAY + " --> " + ChatColor.RED + BankUtil.getIdToString(tra.getTargetId()) + ChatColor.GRAY + " : " + ChatColor.YELLOW + tra.getValue() + "€");
												}
											}
											else {
												player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du hast auf diesem Konto keine Überweisungen erhalten!");
											}
										} catch (SQLException e) {
											player.sendMessage(Bank.PREFIX + ChatColor.RED + "Es ist ein Fehler aufgetreten, bitte melde dies einem Administrator.");
											MySQLConnectionUtils.closeConnection(con);
											return true;
										}
										
										MySQLConnectionUtils.closeConnection(con);
									}
									else {
										player.sendMessage(Bank.PREFIX + ChatColor.RED + "Du besitzt kein Konto mit der Kontonummer " + ChatColor.YELLOW + args[3]);
									}
								}
								else {
									player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank account transactions <sent|received> <Kontonummer>");
								}
							}
							else {
								player.sendMessage(Bank.PREFIX + ChatColor.RED + "/bank account transactions <sent|received> <Kontonummer>");
							}
							
						} else { sendHelp(player); }
					} else { sendHelp(player); }
				} else { sendHelp(player); }
			} else { sendHelp(player); }
		}
		return true;
	}
	
	private void sendHelp(Player player) {
		player.sendMessage(Bank.PREFIX + ChatColor.GOLD + "Hilfe" + ChatColor.DARK_GRAY + ":");
		player.sendMessage(ChatColor.GREEN + "/bank balance" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Zeige dein Bargeld");
		player.sendMessage(ChatColor.GREEN + "/bank pay <Spieler> <Anzahl>" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Schicke einem Spieler Geld");
		player.sendMessage(ChatColor.GREEN + "/bank account create" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Erstelle ein neues Konto");
		player.sendMessage(ChatColor.GREEN + "/bank account delete <Kontonummer>" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Lösche ein vorhandenes Konto");
		player.sendMessage(ChatColor.GREEN + "/bank account deposit <Kontonummer> <Anzahl>" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Zahle Geld auf ein Konto ein");
		player.sendMessage(ChatColor.GREEN + "/bank account payoff <Kontonummer> <Anzahl>" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Zeige dein Bargeld");
		player.sendMessage(ChatColor.GREEN + "/bank account balance <Kontonummer>" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Zeige das Geld auf dem Konto");
		player.sendMessage(ChatColor.GREEN + "/bank account list" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Liste deine Konten auf");
		player.sendMessage(ChatColor.GREEN + "/bank account transfer <Kontonummer> <Zielkontonummer> <Anzahl>" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Überweise Geld auf ein anderes Konto");
		player.sendMessage(ChatColor.GREEN + "/bank account  transactions <sent|received> <Kontonummer>" + ChatColor.GRAY + " - " + ChatColor.GOLD + "Zeigt die Überweisungen eines Kontos");
	}

}
